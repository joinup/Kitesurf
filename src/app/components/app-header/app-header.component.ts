import { Component } from '@angular/core';
import { LoginService } from '../../services/login/login.service';


@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html'
})
export class AppHeaderComponent{ 

	constructor(public loginService : LoginService) {

	}

	public salir(){
		this.loginService.logout();
	}

}
