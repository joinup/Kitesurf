import { Component, OnInit, Input ,Output, EventEmitter} from '@angular/core';
import { TorneoService } from '../../services/torneo/torneo.service';

import { Torneo } from '../../class/torneo';

@Component({
  selector: 'app-select-torneos',
  templateUrl: './select-torneos.component.html',
  styleUrls: ['./select-torneos.component.scss']
})
export class SelectTorneosComponent implements OnInit {

  @Output() resp: EventEmitter <any> = new EventEmitter <any>();
  public torneos: Array<Torneo>;
  public torneo : Torneo;

  constructor( public torneoService: TorneoService ) { }

  ngOnInit() {
  	this.getTorneos();
  }

  public change() {
    console.log(this.torneo)
    this.resp.emit(this.torneo);
  }

  public getTorneos(){
    this.torneoService.getTorneos().subscribe(
      result => {
        this.torneos = result;
      },
      error => {
        console.log(error);
      }
    );
  }



}
