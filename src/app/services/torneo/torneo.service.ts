import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { WebService } from '../web.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';



@Injectable()
export class TorneoService extends WebService {

	getTorneos(): Observable<any>{
		return this.get(`torneo`);
	}
	
	getTorneo(id: number): Observable<any>{
		return this.get(`torneo/${id}`);
	}

   	postTorneo(torneo: Object): Observable<any>{
        return this.post(`torneo`, torneo);
    }

    deleteTorneo(id: number): Observable<any>{
        return this.delete(`torneo/${id}`);
    }

    getNumberParticipante(id: number, numero:number): Observable<any>{
        return this.get(`torneo/${id}/participante/${numero}`);
    }

}
