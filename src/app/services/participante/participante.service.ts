import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { WebService } from '../web.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';



@Injectable()
export class ParticipanteService extends WebService {


	//extraer los participantes de un evento
	getParticipantesEvento(id_evento: number): Observable<any>{
		return this.get(`eventos/${id_evento}/participante`);
	}

	//extraer los participante de un torneo
	getParticipantesTorneo(id_torneo: number): Observable<any>{
		return this.get(`torneo/${id_torneo}/participante`);
	}

	//Guardar participante
	postParticipante(id_torneo: number, participante: Object):Observable<any>{
		return this.post(`torneo/${id_torneo}/participante`, participante);
	}

	//Activar participante
	actParticipante(id: number):Observable<any>{
		return this.put(`participante/${id}/activar`);
	}

	//Desactivar participante
	desactParticipante(id: number):Observable<any>{
		return this.put(`participante/${id}/desactivar`);
	}
	
	//Actualizar Puntuacion
	updateParticipante(id_evento: number, id_participante: number, participante : Object):Observable<any>{
		return this.put(`eventos/${id_evento}/participante/${id_participante}/puntaje`, participante);
	}

}

