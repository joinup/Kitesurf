import { Injectable, ErrorHandler } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { tokenNotExpired, JwtHelper } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';
import { WebService } from '../web.service';

@Injectable()
export class LoginService extends WebService implements CanActivate{

  jwtHelper: JwtHelper = new JwtHelper();

  getToken(): string {
    return localStorage.getItem('token');
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.logged()) {
      // logged in so return true
      return true;
    }
    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    return false;
  }


  useJwtHelper() {
    var token = localStorage.getItem('token');
/*
    console.log(
      this.jwtHelper.decodeToken(token),
      this.jwtHelper.getTokenExpirationDate(token),
      this.jwtHelper.isTokenExpired(token)
    );*/
  }

    //Login
  login(usuario: Object): Observable<any>{
    return this.post(`login`, usuario);
  }

  logged(): boolean{
    return tokenNotExpired();
  }


  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);

  }

}
