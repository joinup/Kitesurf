import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { WebService } from '../web.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';



@Injectable()
export class EventoService extends WebService {

	getEventos(id_torneo : number): Observable<any>{
		return this.get(`torneo/${id_torneo}/eventos`);
	}

	getEvento(id : number): Observable<any>{
		return this.get(`eventos/${id}`);
	}

   	postEvento(id : number ,evento: Object): Observable<any>{
        return this.post(`torneo/${id}/eventos`, evento);
    }

    deleteEvento(id : number): Observable<any>{
      return this.delete(`eventos/${id}`);
    }

}
