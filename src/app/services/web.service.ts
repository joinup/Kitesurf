import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';

@Injectable()
export class WebService {

  public apiUrl = environment.API_ROOT;
  public headers: HttpHeaders;
  constructor(public http: HttpClient, public router: Router, public injector: Injector) {
    this.refresh();
  }

  refresh(){
      this.headers = new HttpHeaders().set('Content-Type','application/json');
      let token = localStorage.getItem('token');
      if(token) {
        this.headers.append('Authorization', `Bearer ${token}`);
      }
  }

  get(path: string, params?:string): Observable<any>{
    this.refresh();
    return this.http.get(this.apiUrl + path, {headers: this.headers});
  }

  post(path: string, data: Object): Observable<any>{
    this.refresh();
    let body = JSON.stringify(data);
    return this.http.post(this.apiUrl + path, body, {headers: this.headers})
  }

  put(path: string, data?: Object): Observable<any>{
    this.refresh();
    let body = data?JSON.stringify(data):'';
    return this.http.put(this.apiUrl + path, body, {headers: this.headers})
  }

  delete(path: string): Observable<any>{
    this.refresh();
    return this.http.delete(this.apiUrl + path, {headers: this.headers});
  }
}
