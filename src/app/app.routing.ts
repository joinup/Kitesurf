import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ParticipantesComponent } from './views/participantes/participantes.component';
import { EventosComponent } from './views/eventos/eventos.component';
import { TorneosComponent } from './views/torneos/torneos.component';
import { PartEventoComponent } from './views/part-evento/part-evento.component';
import { FrontParticipantesComponent } from './views/front-participante/front-participantes.component';
import { PuntuacionesComponent } from './views/puntuaciones/puntuaciones.component';
import { LoginComponent } from './views/login/login.component';
import { PuntuacionesTorneoComponent } from './views/puntuaciones-torneo/puntuaciones-torneo.component';

import { LoginService } from './services/login/login.service';
// Import Containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';

export const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register/:id',
    component: FrontParticipantesComponent,
  },
  {
    // path: 'puntuaciones',
    path: 'puntuaciones/:id', 
    component: PuntuacionesComponent,
  },
   {
    // path: 'puntuaciones',
    path: 'torneo/:id/puntuaciones', 
    component: PuntuacionesTorneoComponent,
  },
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [LoginService],
    data: {
      title: 'Inicio'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'participantes',
        component: ParticipantesComponent,
        data: {
          title: 'Participantes'
        }  
      },
      {
        path: 'eventos',
        component: EventosComponent,
        data: {
          title: 'Eventos'
        }  
      },
      {
        path: 'torneos',
        component: TorneosComponent,
        data: {
          title: 'Torneos'
        }  
      },
      
      {
        path: 'eventos/:id/participantes', 
        component: PartEventoComponent,
      },
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes),RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
