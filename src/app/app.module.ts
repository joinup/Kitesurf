import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { FormsModule, ReactiveFormsModule, FormControlDirective  } from '@angular/forms';
import { JwtModule } from '@auth0/angular-jwt';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';


// Import containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';

const APP_CONTAINERS = [
  FullLayoutComponent,
  SimpleLayoutComponent
]

// Import components
import {
  AppAsideComponent,
  AppBreadcrumbsComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV
} from './components';

const APP_COMPONENTS = [
  AppAsideComponent,
  AppBreadcrumbsComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV
]

// Import directives
import {
  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
} from './directives';

const APP_DIRECTIVES = [
  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
]

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ParticipantesComponent } from './views/participantes/participantes.component';
import { TorneosComponent } from './views/torneos/torneos.component';
import { EventosComponent } from './views/eventos/eventos.component';
import { FormParticipantesComponent } from './views/participantes/form-participantes/form-participantes.component';
import { FormEventoComponent } from './views/eventos/form-evento/form-evento.component';
import { FormTorneoComponent } from './views/torneos/form-torneo/form-torneo.component';
import { FrontParticipantesComponent } from './views/front-participante/front-participantes.component';
import { PartEventoComponent } from './views/part-evento/part-evento.component';
import { SelectTorneosComponent } from './components/select-torneo/select-torneos.component';
import { PuntuacionesComponent } from './views/puntuaciones/puntuaciones.component';
import { EditarPuntuacionComponent } from './views/part-evento/editar-puntuacion/editar-puntuacion.component';
import { PuntuacionesTorneoComponent } from './views/puntuaciones-torneo/puntuaciones-torneo.component';

// providers services
import { WebService } from './services/web.service';
import { EventoService } from './services/evento/evento.service';
import { ParticipanteService } from './services/participante/participante.service';
import { TorneoService } from './services/torneo/torneo.service';
import { LoginService } from './services/login/login.service';
import { AuthErrorHandler } from './services/login/auth-error-handler.service';

import { MessageService } from 'primeng/components/common/messageservice';


import {CalendarModule,
        DataTableModule,
        DropdownModule,
        GrowlModule,
        DialogModule,
        MultiSelectModule
} from 'primeng/primeng';
import { LoginComponent } from './views/login/login.component';

export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    ReactiveFormsModule,
    FormsModule, 
    CalendarModule,
    BrowserAnimationsModule,
    DataTableModule,
    DropdownModule,
    HttpClientModule,
    GrowlModule,
    DialogModule,
    MultiSelectModule,
    JwtModule.forRoot({
      config: {
        tokenGetter,
        whitelistedDomains: ['localhost:8000','https://api-vzlakite.herokuapp.com']
      }
    })
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    ...APP_COMPONENTS,
    ...APP_DIRECTIVES,
    ParticipantesComponent,
    TorneosComponent,
    EventosComponent,
    FormParticipantesComponent,
    FormEventoComponent,
    FormTorneoComponent,
    FrontParticipantesComponent,
    PartEventoComponent,
    EditarPuntuacionComponent,
    SelectTorneosComponent,
    PuntuacionesComponent,
    LoginComponent,
    PuntuacionesTorneoComponent,
  ],
  providers: [
    WebService,
    EventoService,
    ParticipanteService,
    TorneoService,
    LoginService,
    MessageService,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    // {
    //   provide: ErrorHandler, 
    //   useClass: AuthErrorHandler
    // }
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
