import { Component, OnInit } from '@angular/core';
import { Torneo } from '../../class/torneo';
import { EventoService } from '../../services/evento/evento.service';
import { TorneoService } from '../../services/torneo/torneo.service';
import { ParticipanteService } from '../../services/participante/participante.service';
import {ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-puntuaciones',
  templateUrl: './puntuaciones.component.html',
  styleUrls: ['./puntuaciones.component.scss']
})
export class PuntuacionesComponent implements OnInit {

  public eventos: Array<any>;
  public participantesPunt: Array <any>;
  public torneo : number;
  public evento : number;
  public detalleTorneo: Torneo;
  
  constructor(public eventService : EventoService,
            public participanteService : ParticipanteService,
            private route: ActivatedRoute,
            public torneoService : TorneoService) {
    this.route.params.subscribe(
       params => this.torneo = params.id
     );
  }

  ngOnInit() {
    this.getEventos();
    this.getTorneo();
  }

  public getEventos(){
    this.eventService.getEventos(this.torneo).subscribe(
      result => {
        this.eventos = result;
        if (this.eventos.length >= 1) {
          this.evento = this.eventos[0].id;
        }else{
          this.evento = this.eventos[this.eventos.length - 1].id;
        }
        this.participantes();
      },
      error => {
        console.log(error);
      }
    );
  }

  public getTorneo(){
    this.torneoService.getTorneo(this.torneo).subscribe(
      result => {
        this.detalleTorneo = result;
      },
      error => {
        console.log(error);
      }
    );
  }


  public change(){
    this.participantes();
  }

  public participantes(){
    this.participanteService.getParticipantesEvento(this.evento).subscribe(
      result => {
        this.participantesPunt = result;
      },
      error => {
        console.log(error);
      }
    );
  }

}
