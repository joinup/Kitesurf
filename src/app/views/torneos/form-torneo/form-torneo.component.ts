import { Component, OnInit,  Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl} from '@angular/forms';

import { calendarEs }  from '../../../app.component';
import * as moment from 'moment';


import { TorneoService } from '../../../services/torneo/torneo.service';
import { MessageService } from 'primeng/components/common/messageservice';



@Component({
  selector: 'app-form-torneo',
  templateUrl: './form-torneo.component.html',
  styleUrls: ['./form-torneo.component.scss']
})
export class FormTorneoComponent implements OnInit {

  @Output() resp: EventEmitter<object> = new EventEmitter<object>();
  public formTorneo: FormGroup;
  public es = calendarEs;
  public minDate = new Date();

  constructor(private fb:FormBuilder, public torneoService : TorneoService, public messageService: MessageService) {

  	this.formTorneo = this.fb.group({
      'nombre': ['',Validators.compose([Validators.required])],
  	  'fecha': ['',Validators.compose([Validators.required])],
      'descripcion': ['', Validators.compose([Validators.required, Validators.minLength(10)])],
      'direccion': ['', Validators.compose([Validators.required, Validators.minLength(10)])],
  	});

  }

  ngOnInit() {
    this.minDate.setDate(this.minDate.getDate() + 1);
  }

  public registrarTorneo(values) {
      values.fecha = moment(values.fecha).format('Y-MM-DD');

      if (this.formTorneo.valid) {
        this.torneoService.postTorneo(values).toPromise().then(
          response => {
              this.messageService.add({severity:'success', summary:'Torneo guardado con exito'});
              this.resp.emit();
              this.formTorneo.reset();
          }, 
          error => {
              console.log(error);
              this.messageService.add({severity:'error', summary:'Error'});
          }
        );
      }else {
        this.messageService.add({severity:'error', summary:'Complete los campos'});
    }
  } 


}
