import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTorneoComponent } from './form-torneo.component';

describe('FormTorneoComponent', () => {
  let component: FormTorneoComponent;
  let fixture: ComponentFixture<FormTorneoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormTorneoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTorneoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
