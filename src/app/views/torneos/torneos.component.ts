import { Component, OnInit } from '@angular/core';
import { TorneoService } from '../../services/torneo/torneo.service';

import {Message} from 'primeng/primeng';
import {MessageService} from 'primeng/components/common/messageservice';


@Component({
  selector: 'app-torneos',
  templateUrl: './torneos.component.html',
  styleUrls: ['./torneos.component.scss']
})
export class TorneosComponent implements OnInit {

	public torneos: Array<any>;
	public torneo : any;
	public new: boolean = false;

  constructor( public torneoService: TorneoService,
               public messageService: MessageService) { }

  ngOnInit() {
  	this.getTorneos();
  }

  public saved() {
    this.new = false;
    this.getTorneos();
  }

  public newTorneo(){
  	this.new = true;			//Permitimos crear un nuevo torneo
  	this.torneo = null;	//Los campos del torneo vacio
  }

  public getTorneos(){
    this.torneoService.getTorneos().subscribe(
      result => {
        this.torneos = result;
      },
      error => {
        console.log(error);
      }
    );
  }

  public deleteTorneo(id){
    this.torneoService.deleteTorneo(id.id).subscribe(
      result => {
        this.getTorneos();
         this.messageService.add({severity:'success', summary:'Torneo eliminado con exito'});
      },
      error => {
        console.log(error);
      }
    );
  }


}
