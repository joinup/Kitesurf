import { Component, OnInit } from '@angular/core';
import { ParticipanteService } from '../../services/participante/participante.service';
import { Torneo } from '../../class/torneo';
import {ActivatedRoute } from '@angular/router';
import { TorneoService } from '../../services/torneo/torneo.service';


@Component({
  selector: 'app-front-participantes',
  templateUrl: './front-participantes.component.html',
  styleUrls: ['./front-participantes.component.scss']
})
export class FrontParticipantesComponent implements OnInit {

	public participantes: Array<any>;
	public participante: any;
	public new: boolean = false;
  public display: boolean = false;
  public torneo_parametro: number;
  public torneo: Torneo;

  constructor( public participanteService : ParticipanteService,
               private route: ActivatedRoute,
               public torneoService : TorneoService) { 
    this.route.params.subscribe(
       params => this.torneo_parametro = params.id
     );

  }

  ngOnInit() {
    this.getTorneo();
  }

  public saved() {
    this.new = false;
  }

  public getTorneo(){
    this.torneoService.getTorneo(this.torneo_parametro).subscribe(
      result => {
        this.torneo = result;
      },
      error => {
        console.log(error);
      }
    );
  }




}
