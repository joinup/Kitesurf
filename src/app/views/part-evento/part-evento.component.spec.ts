import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartEventoComponent } from './part-evento.component';

describe('PartEventoComponent', () => {
  let component: PartEventoComponent;
  let fixture: ComponentFixture<PartEventoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartEventoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartEventoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
