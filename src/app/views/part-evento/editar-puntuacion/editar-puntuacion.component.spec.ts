import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarPuntuacionComponent } from './editar-puntuacion.component';

describe('EditarPuntuacionComponent', () => {
  let component: EditarPuntuacionComponent;
  let fixture: ComponentFixture<EditarPuntuacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarPuntuacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarPuntuacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
