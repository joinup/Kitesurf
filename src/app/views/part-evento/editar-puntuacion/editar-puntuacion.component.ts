import { Component, OnInit, OnChanges,  Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl} from '@angular/forms';
import { ParticipanteService } from '../../../services/participante/participante.service';

import {MessageService} from 'primeng/components/common/messageservice';


@Component({
  selector: 'app-editar-puntuacion',
  templateUrl: './editar-puntuacion.component.html',
  styleUrls: ['./editar-puntuacion.component.scss']
})
export class EditarPuntuacionComponent implements OnInit {

  @Output() resp: EventEmitter<object> = new EventEmitter<object>();
  @Input() participante: any;  
	@Input() evento: any;  

  public formParticipante: FormGroup;
  public nombre: AbstractControl;
  public apellido: AbstractControl;
  public cedula : AbstractControl;
  public puntuacion: AbstractControl;


  constructor(private fb:FormBuilder,
              public participanteService : ParticipanteService,
              public messageService : MessageService) { 
     this.formParticipante = this.fb.group({
          'nombre': [''],
          'apellido': [''],
          'cedula': [''],
          'puntuacion': [''],
         });

    this.nombre = this.formParticipante.controls['nombre'];
    this.apellido = this.formParticipante.controls['apellido'];
    this.cedula = this.formParticipante.controls['cedula'];
    this.puntuacion = this.formParticipante.controls['puntuacion'];

  }

  ngOnInit() {
  }

  ngOnChanges() {
      if (this.participante) {
          this.nombre.setValue(this.participante.nombre);
          this.apellido.setValue(this.participante.apellido);
          this.cedula.setValue(this.participante.cedula);
          this.puntuacion.setValue(this.participante.puntuacion);
      } else {
          this.formParticipante.reset();
      }
  }

  public editar_puntuacion(values){
    if (this.formParticipante.valid) {
      this.participanteService.updateParticipante(this.evento, this.participante.id, values).toPromise().then(
          response => {
              this.messageService.add({severity:'success', summary:'Puntaje actualizado'});
              this.resp.emit();
              this.formParticipante.reset();
          }, 
          error => {
              console.log(error);
              this.messageService.add({severity:'error', summary:'Error'});
          }
        );
      }else {
        this.messageService.add({severity:'error', summary:'Complete los campos'});
    }

  }
}


