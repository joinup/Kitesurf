import { Component, OnInit } from '@angular/core';
import { ParticipanteService } from '../../services/participante/participante.service';
import {ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-part-evento',
  templateUrl: './part-evento.component.html',
  styleUrls: ['./part-evento.component.scss']
})
export class PartEventoComponent implements OnInit {

	public participantes: Array<any>;
	public evento : number;

	public participante: any;
	public editar_puntuacion: boolean = false;

  constructor(private route: ActivatedRoute, public participanteService : ParticipanteService) {
  	 this.route.params.subscribe(
  	 	params => this.evento = params.id
  	 );
   }

  ngOnInit() {
  	this.getPaticipantes();
  }

  public getPaticipantes(){
    this.participanteService.getParticipantesEvento(this.evento).subscribe(
      result => {
        this.participantes = result;
      },
      error => {
        console.log(error);
      }
    );
  }

  public editar(participante){
  	this.editar_puntuacion = true;			//Permitimos crear un nuevo usuario
  	this.participante = participante;	//Los campos del participante vacio
  }

  public saved() {
    this.editar_puntuacion = false;
    this.getPaticipantes();
  }
          
}
