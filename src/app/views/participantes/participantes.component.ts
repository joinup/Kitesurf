import { Component, OnInit } from '@angular/core';
import { ParticipanteService } from '../../services/participante/participante.service';
import { Torneo } from '../../class/torneo';


@Component({
  selector: 'app-participantes',
  templateUrl: './participantes.component.html',
  styleUrls: ['./participantes.component.scss']
})
export class ParticipantesComponent implements OnInit {

	public participantes: Array<any>;
	public participante: any;
	public new: boolean = false;
  public display: boolean = false;
  public torneo: Torneo;

  constructor( public participanteService : ParticipanteService) { }

  ngOnInit() {
  }

  public saved() {
    this.new = false;
    this.getPaticipantes();
  }

  public newParticipante(){
  	this.new = true;			//Permitimos crear un nuevo usuario
  	this.participante = null;	//Los campos del participante vacio
  }

  public selected_torneo(torneo: Torneo) {
    this.torneo = torneo;
    this.getPaticipantes()
  }

  public getPaticipantes(){
    this.participanteService.getParticipantesTorneo(this.torneo.id).subscribe(
      result => {
        this.participantes = result;
        this.agrupar();
      },
      error => {
        console.log(error);
      }
    );
  }

  public changeEstado(participante){
    if (participante.estado) {
      this.participanteService.desactParticipante(participante.id).subscribe(
        result => {
          //this.participantes = result;
          this.getPaticipantes();
        },
        error => {
          console.log(error);
        }
      );
    }else{
      this.participanteService.actParticipante(participante.id).subscribe(
        result => {
          //this.participantes = result;
          this.getPaticipantes();
        },
        error => {
          console.log(error);
        }
      );

    }

  }

  moreInfo(participante) {
    this.display = true;

    this.participante = participante;
    console.log(participante);
    console.log(participante.nombre);
  }

  public agrupar(){
    for (var i = this.participantes.length - 1; i >= 0; i--) {
      this.participantes[i].cedula=this.participantes[i].nacionalidad+'-'+this.participantes[i].cedula;
      this.participantes[i].nombre=this.participantes[i].nombre+' '+this.participantes[i].apellido;
    }
  }


}
