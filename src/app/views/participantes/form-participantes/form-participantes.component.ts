import { Component, OnInit, OnChanges,  Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl} from '@angular/forms';
import { FormControlName } from '@angular/forms';

import { calendarEs }  from '../../../app.component';

import * as moment from 'moment';

import { ParticipanteService } from '../../../services/participante/participante.service';
import { MessageService } from 'primeng/components/common/messageservice';

import { EventoService } from '../../../services/evento/evento.service';
import { TorneoService } from '../../../services/torneo/torneo.service';

import { Torneo } from '../../../class/torneo';

@Component({
  selector: 'app-form-participantes',
  templateUrl: './form-participantes.component.html',
  styleUrls: ['./form-participantes.component.scss']
})
export class FormParticipantesComponent implements OnChanges {
    @Output() resp: EventEmitter<object> = new EventEmitter<object>();
    @Input() torneo: Torneo;  
    public participantes: Array<any>;
    public formParticipante: FormGroup;

    ///******** Form *******////
    public nombre: AbstractControl;
    public apellido: AbstractControl;
    public cedula: AbstractControl;
    public nacionalidad: AbstractControl;
    public estado_pais: AbstractControl;
    public fecha_nacimiento: AbstractControl;
    public sexo: AbstractControl;
    public telefono: AbstractControl;
    public correo: AbstractControl;
    public talla: AbstractControl;
    public club: AbstractControl;
    public apodo: AbstractControl;
    public numero: AbstractControl;
    public tipo_sangre: AbstractControl;
    public contacto_emer: AbstractControl;
    public tlf_emer: AbstractControl;
    public alergias: AbstractControl;
    public antecedentes: AbstractControl;
    public instagram: AbstractControl;
    public facebook: AbstractControl;
    public twitter: AbstractControl;
    public youtube: AbstractControl;
    public evento: AbstractControl;
    ///******** Form *******////

    public validFormSubmitted = false;
    public maxDate = new Date();

    public es = calendarEs;

    public numeroValido = true;

    public estados = [
        {'nombre':'Amazonas','id':'01'},
        {'nombre':'Anzoátegui','id':'02'},
        {'nombre':'Apure','id':'03'},
        {'nombre':'Aragua','id':'04'},
        {'nombre':'Barinas','id':'05'},
        {'nombre':'Bolívar','id':'06'},
        {'nombre':'Carabobo','id':'07'},
        {'nombre':'Cojedes','id':'08'},
        {'nombre':'Delta Amacuro','id':'09'},
        {'nombre':'Distrito Capital','id':'10'},
        {'nombre':'Falcón', 'id':'11'},
        {'nombre':'Guárico', 'id':'12'},
        {'nombre':'Lara', 'id':'13'},
        {'nombre':'Mérida', 'id':'14'},
        {'nombre':'Miranda', 'id':'15'},
        {'nombre':'Monagas', 'id':'16'},
        {'nombre':'Nueva Esparta', 'id':'17'},
        {'nombre':'Portuguesa', 'id':'18'},
        {'nombre':'Sucre', 'id':'19'},
        {'nombre':'Táchira', 'id':'20'},
        {'nombre':'Trujillo','id':'21'},
        {'nombre':'Vargas','id':'22'},
        {'nombre':'Yaracuy','id':'23'},
        {'nombre':'Zulia','id':'24'},
    ];

    public eventos: Array<any>;

    public tipos_sangre = [{'nombre':'A+','id':'01'},
                        {'nombre':'A-','id':'02'},
                        {'nombre':'B+','id':'03'},
                        {'nombre':'B-','id':'04'},
                        {'nombre':'AB+','id':'05'},
                        {'nombre':'AB-','id':'06'},
                        {'nombre':'O+','id':'07'},
                        {'nombre':'O-','id':'08'}];
    

    constructor(
        private fb:FormBuilder,
        public participanteService : ParticipanteService,
        public messageService: MessageService,
        public eventService: EventoService,
        public torneoService: TorneoService
    ) {
        this.formParticipante = this.fb.group({
          'nombre': ['',Validators.compose([Validators.required, Validators.minLength(3)])],
          'apellido': ['', Validators.compose([Validators.required, Validators.minLength(3)])],
          'cedula': ['', Validators.compose([Validators.required, Validators.minLength(7)])],
          'nacionalidad': ['', Validators.compose([Validators.required])],
          'estado_pais': ['', Validators.compose([Validators.required, Validators.minLength(5)])],
          'fecha_nacimiento': ['', Validators.compose([Validators.required])],
          'sexo': ['', Validators.compose([Validators.required])],
          'telefono': ['', Validators.compose([Validators.required, Validators.minLength(10)])],
          'correo': ['', Validators.compose([Validators.required, Validators.minLength(10)])],
          'talla': ['', ],
          'club': ['', ],
          'apodo': ['', ],
          'numero': ['', ],
          'tipo_sangre': ['',],
          'contacto_emer': ['', ],
          'tlf_emer': ['', ],
          'alergias': ['', ],
          'antecedentes': ['', ],
          'instagram': ['', ],
          'facebook': ['', ],
          'twitter': ['', ],
          'youtube': ['', ],
          'evento': ['', ]
        });
        this.nombre = this.formParticipante.controls['nombre']
        this.apellido = this.formParticipante.controls['apellido']
        this.cedula = this.formParticipante.controls['cedula']
        this.nacionalidad = this.formParticipante.controls['nacionalidad']
        this.estado_pais = this.formParticipante.controls['estado_pais']
        this.fecha_nacimiento = this.formParticipante.controls['fecha_nacimiento']
        this.sexo = this.formParticipante.controls['sexo']
        this.telefono = this.formParticipante.controls['telefono']
        this.correo = this.formParticipante.controls['correo']
        this.talla = this.formParticipante.controls['talla']
        this.club = this.formParticipante.controls['club']
        this.apodo = this.formParticipante.controls['apodo']
        this.numero = this.formParticipante.controls['numero']
        this.tipo_sangre = this.formParticipante.controls['tipo_sangre']
        this.contacto_emer = this.formParticipante.controls['contacto_emer']
        this.tlf_emer = this.formParticipante.controls['tlf_emer']
        this.alergias = this.formParticipante.controls['alergias']
        this.antecedentes = this.formParticipante.controls['antecedentes']
        this.instagram = this.formParticipante.controls['instagram']
        this.facebook = this.formParticipante.controls['facebook']
        this.twitter = this.formParticipante.controls['twitter']
        this.youtube = this.formParticipante.controls['youtube']
        this.evento = this.formParticipante.controls['evento']
    }

    ngOnChanges() {
      this.getEventos();
      
    }


   public onSubmit(values) {
    console.log(values)
    this.validFormSubmitted = true;
    values.fecha_nacimiento = moment(values.fecha_nacimiento).format('Y-MM-DD');
    if (this.formParticipante.valid) {
      this.participanteService.postParticipante(this.torneo.id, values).toPromise().then(
        response => {
            this.messageService.add({severity:'success', summary:'Participante guardado con exito'});
            this.resp.emit();
            this.formParticipante.reset();
            // this.getParticipantes();
        }, 
        error => {
            console.log(error);
            this.messageService.add({severity:'error', summary:'Error'});
        }
      );
    }else {
      this.messageService.add({severity:'error', summary:'Complete los campos'});
    }
  } 

  public getEventos(){
    this.eventService.getEventos(this.torneo.id).subscribe(
      result => {
        this.eventos = result;
      },
      error => {
        console.log(error);
      }
    );
  }

  public validarParticipante(numero){
    if (numero.value != "") {
      this.torneoService.getNumberParticipante(this.torneo.id,numero.value).subscribe(
        result => {
          this.numeroValido = result;
          if (this.numeroValido == true) {
            this.messageService.add({severity:'success', summary:'Número disponible'});
          }else{
            this.messageService.add({severity:'error', summary:'Este Número ya se encuentra registrado'});
          }
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  /*public getParticipantes(){
    this.participanteService.getParticipantesTorneo(this.torneo.id).subscribe(
      result => {
        this.participantes = result;
      },
      error => {
        console.log(error);
      }
    );
  }*/


}
