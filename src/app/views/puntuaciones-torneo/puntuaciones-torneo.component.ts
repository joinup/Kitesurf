import { Component, OnInit } from '@angular/core';
import { Torneo } from '../../class/torneo';
import {ActivatedRoute } from '@angular/router';
import { ParticipanteService } from '../../services/participante/participante.service';
import { TorneoService } from '../../services/torneo/torneo.service';

@Component({
  selector: 'app-puntuaciones-torneo',
  templateUrl: './puntuaciones-torneo.component.html',
  styleUrls: ['./puntuaciones-torneo.component.scss']
})
export class PuntuacionesTorneoComponent implements OnInit {

  public participantesPunt: Array <any>;
  public torneo : number;
  public detalleTorneo: Torneo;


  constructor(public participanteService : ParticipanteService,
            private route: ActivatedRoute,
            public torneoService : TorneoService) { 

   	this.route.params.subscribe(
       params => this.torneo = params.id
    );}

  ngOnInit() {
    this.getTorneo();
    this.puntuaciones();
  }

  public getTorneo(){
    this.torneoService.getTorneo(this.torneo).subscribe(
      result => {
        this.detalleTorneo = result;
      },
      error => {
        console.log(error);
      }
    );
  }

  public puntuaciones(){
    this.participanteService.getParticipantesTorneo(this.torneo).subscribe(
      result => {
        this.participantesPunt = result;
      },
      error => {
        console.log(error);
      }
    );
  }

}
