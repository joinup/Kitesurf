import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PuntuacionesTorneoComponent } from './puntuaciones-torneo.component';

describe('PuntuacionesTorneoComponent', () => {
  let component: PuntuacionesTorneoComponent;
  let fixture: ComponentFixture<PuntuacionesTorneoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PuntuacionesTorneoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PuntuacionesTorneoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
