import { Component, OnInit } from '@angular/core';
import { EventoService } from '../../services/evento/evento.service';
import { TorneoService } from '../../services/torneo/torneo.service';
import { Torneo } from '../../class/torneo';
import { calendarEs }  from '../../app.component';

import {ActivatedRoute } from '@angular/router';
import {Router} from "@angular/router";




@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.scss']
})
export class EventosComponent implements OnInit {

  public eventos: Array<any>;
  public evento: any;
  public new: boolean = false;


  public torneo: Torneo;
  constructor(public eventService: EventoService, 
              public torneoService:TorneoService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    
  }

  public saved() {
       this.new = false;
       this.getEventos(this.torneo.id);
  }

  public newEvento(){
    this.new = true;
    this.evento = null;
  }

  public selected_torneo(torneo: Torneo) {
    this.torneo = torneo;
    this.getEventos(this.torneo.id);
  }

  public getEventos(torneo){
    this.eventService.getEventos(torneo).subscribe(
      result => {
        this.eventos = result;
      },
      error => {
        console.log(error);
      }
    );
  }

  public deleteEvento(id){
    this.eventService.deleteEvento(id.id).subscribe(
      result => {
        this.getEventos(this.torneo.id);
      },
      error => {
        console.log(error);
      }
    );
  }


  public participantes(id:string) {
    this.router.navigate(['eventos', id ,'participantes']); 
  }




}
