import { Component, OnInit,  OnChanges, Input, Output, EventEmitter  } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl} from '@angular/forms';

import { calendarEs }  from '../../../app.component';
import * as moment from 'moment';

import { EventoService } from '../../../services/evento/evento.service';
import {MessageService} from 'primeng/components/common/messageservice';

import { TorneoService } from '../../../services/torneo/torneo.service';
import { Torneo } from '../../../class/torneo';


@Component({
  selector: 'app-form-evento',
  templateUrl: './form-evento.component.html',
  styleUrls: ['./form-evento.component.scss']
})
export class FormEventoComponent implements OnInit {
  @Output() resp: EventEmitter<object> = new EventEmitter<object>();
  @Input() torneo: Torneo; 
  @Input() event: any;

  public formEvento: FormGroup;
  public es = calendarEs;
  public minDate = new Date();


  constructor(private fb:FormBuilder,
              public eventoService : EventoService, 
              public msg : MessageService,
              public torneoService: TorneoService ) { 

  	this.formEvento = this.fb.group({
      'nombre': ['',Validators.compose([Validators.required])],
	    'fecha': ['',Validators.compose([Validators.required])],
      'descripcion': ['', Validators.compose([Validators.required, Validators.minLength(10)])],
      'direccion': ['', Validators.compose([Validators.required, Validators.minLength(10)])],
  	});

  }

  ngOnInit() {
    this.minDate.setDate(this.minDate.getDate() + 1);
  }

  public registrar_evento(values) {
    values.fecha = moment(values.fecha).format('Y-MM-DD');

    if (this.formEvento.valid) {
      this.eventoService.postEvento(this.torneo.id,values).toPromise().then(
        response => {
            this.msg.add({severity:'success', summary:'Evento guardado con exito'});
            this.resp.emit();
            this.formEvento.reset();
        }, 
        error => {
            console.log(error);
            this.msg.add({severity:'error', summary:'Error'});
        }
      );
    }else {
      this.msg.add({severity:'error', summary:'Complete los campos'});
    }
  }
}

