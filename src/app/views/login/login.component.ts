import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { LoginService } from '../../services/login/login.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { tokenNotExpired } from 'angular2-jwt';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public form : FormGroup;

  constructor( 
          fb: FormBuilder,
          public loginService: LoginService,
          public messageService : MessageService,
          public router: Router,
  ) { 
    this.form = fb.group({
        'email': ['', Validators.compose([Validators.required])],
        'password': ['', Validators.compose([Validators.required])],
    });

  }

  ngOnInit() {
    this.logueado();
  }

  public onSubmit(values){
    if (this.form.valid) {
      this.loginService.login(values).toPromise().then(
        response => {
            this.messageService.add({severity:'success', summary:'Bienvenido'});
            this.form.reset();
            localStorage.setItem('token',response.token);
            this.router.navigate(['']);
        }, 
        error => {
            console.log(error);
            this.messageService.add({severity:'error', summary:'Error'});
        }
      );
    }else {
      this.messageService.add({severity:'error', summary:'Complete los campos'});
    }
  }

  logged(): boolean{
    return tokenNotExpired();
  }

  logueado(){
    if (this.logged()) {
      this.router.navigate(['/dashboard']);
    }
  }


}
