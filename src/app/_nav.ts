export const navigation = [
  {
    name: 'Inicio',
    url: '/dashboard',
    icon: 'fa fa-home',
    badge: {
      variant: 'info',
    }
  },

  {
    name: 'Torneos',
    url: '/torneos',
    icon: 'fa fa-trophy',
    badge: {
      variant: 'info',
    }
  },
  {
    name: 'Eventos',
    url: '/eventos',
    icon: 'fa fa-calendar-check-o',
    badge: {
      variant: 'info',
    }
  },

  {
    name: 'Participantes',
    url: '/participantes',
    icon: 'fa fa-user-o',
    badge: {
      variant: 'info',
    }
  }
];
